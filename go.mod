module gitlab.com/NebulousLabs/monitor

go 1.13

require (
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	golang.org/x/crypto v0.0.0-20191105034135-c7e5f84aec59 // indirect
)
